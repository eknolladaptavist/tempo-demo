export const params = {
    source: {
        instanceName: 'paulsaunders',
        cloudIssue: 'SR-715',
        serverIssue: 'FRID-4'
    },
    target: {
        instanceName: 'elrond5',
        project: 'EL'
    }
}