import { getMetadata } from './storage';
import { params } from './params';

// Run this script to print out URLs for setting up Tempo OAuth connections (step 2)
export async function run() {
    const metadata = await getMetadata();

    console.log(`Tempo source: https://${params.source.instanceName}.atlassian.net/plugins/servlet/ac/io.tempo.jira/oauth-authorize/?client_id=${metadata.tempo.source.appClientId}&redirect_uri=${metadata.tempo.oauthReplyUrl}&access_type=tenant_user&state=source`);
    console.log(`Tempo target: https://${params.target.instanceName}.atlassian.net/plugins/servlet/ac/io.tempo.jira/oauth-authorize/?client_id=${metadata.tempo.target.appClientId}&redirect_uri=${metadata.tempo.oauthReplyUrl}&access_type=tenant_user&state=target`);
}