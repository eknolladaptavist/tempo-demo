import JiraServerSource from './connections/jiraServer/source';
import JiraCloudTarget from './connections/jiraCloud/target';
import { getUserMapping, storeUserMapping, UserMapping } from './storage';

import { HttpEndpointRequest, HttpEndpointResponse, buildHTMLResponse } from '@avst-lib/http-endpoint';

export async function run(request: HttpEndpointRequest): Promise<HttpEndpointResponse | void> {
    try {
        const userMapping = await getUserMapping();

        if (request.queryStringParams['sourceUser']) {
            userMapping.push({
                sourceUser: request.queryStringParams['sourceUser'],
                targetUser: request.queryStringParams['targetUser']
            });

            await storeUserMapping(userMapping);
        }

        if (request.queryStringParams['deleteSourceUser']) {
            userMapping.splice(userMapping.findIndex(({ sourceUser }) => sourceUser === request.queryStringParams['deleteSourceUser']), 1);

            await storeUserMapping(userMapping);
        }

        const [sourceUsers, targetUsers] = await Promise.all([getSourceUsers(), getTargetUsers()]);

        return buildHTMLResponse(`
            <style>
                table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
                }
            </style>
            <form>
                <label for="sourceUser">Source user:</label>
                ${getSourceUsersHtml(userMapping, sourceUsers)}<br />
                <label for="targetUser">Target user:</label>
                ${getTargetUsersHtml(targetUsers)}<br />
                <input type="submit" value="Add"/>
            </form>
            ${getUserMappingTableHtml(userMapping, sourceUsers, targetUsers)}
        `);
    } catch (e) {
        console.error('Error while rendering page', e);
        return buildHTMLResponse('Error while rendering page');
    }
}

async function getSourceUsers() {
    return (await JiraServerSource.User.Search.findUsers({ username: '.' })).map(({ key, displayName }) => ({ key, displayName } as SourceUser));
}

async function getTargetUsers() {
    return (await JiraCloudTarget.User.getUsers()).map(({ accountId, displayName }) => ({ accountId, displayName } as TargetUser));
}

function getSourceUsersHtml(userMapping: UserMapping[], sourceUsers: SourceUser[]) {
    const users = sourceUsers.filter(({ key }) => !userMapping.find(({ sourceUser }) => sourceUser === key));

    return `<select name="sourceUser">${users.map(({ displayName, key }) => (`<option value="${key}">${displayName} (${key})</option>`))}</select>`;
}

function getTargetUsersHtml(targetUsers: TargetUser[]) {
    return `<select name="targetUser">${targetUsers.map(({ displayName, accountId }) => (`<option value="${accountId}">${displayName ?? ''} (${accountId})</option>`))}</select>`;
}

function getUserMappingTableHtml(userMapping: UserMapping[], sourceUsers: SourceUser[], targetUsers: TargetUser[]) {
    if (userMapping.length > 0) {
        return `
        <table>
            <thead>
                <tr><td>Source User</td><td>Target User</td><td></td></tr>
            </thead>
            <tbody>
                ${userMapping.map(({ sourceUser, targetUser }) => (`
                    <tr>
                        <td>${sourceUsers.find(({ key }) => key === sourceUser)?.displayName ?? 'MISSING'} (${sourceUser})</td>
                        <td>${targetUsers.find(({ accountId }) => accountId === targetUser)?.displayName ?? 'MISSING'} (${targetUser})</td>
                        <td><form><input type="hidden" name="deleteSourceUser" value="${sourceUser}"/><input type="submit" value="Remove"/></form></td>
                    <tr>
                `)).join('')}
            </tbody>
        </table>
    `;
    } else {
        return '';
    }
}

interface SourceUser {
    key: string;
    displayName: string;
}

interface TargetUser {
    accountId: string;
    displayName: string;
}