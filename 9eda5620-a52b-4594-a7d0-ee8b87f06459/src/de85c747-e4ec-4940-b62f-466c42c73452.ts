import { storeMetadata } from './storage';

// Run this script to set up metadata (step 1)
export async function run() {
    await storeMetadata({
        tempo: {
            oauthReplyUrl: ' ',
            source: {
                appClientId: '',
                appClientSecret: ''
            },
            target: {
                appClientId: '',
                appClientSecret: ''
            }
        }
    });
}