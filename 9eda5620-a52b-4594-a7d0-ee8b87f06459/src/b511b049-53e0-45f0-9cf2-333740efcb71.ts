import JiraCloudSource from './connections/jiraCloud/source';
import JiraCloudTarget from './connections/jiraCloud/target';
import { getAuthHeaders } from './connections/tempo';
import { params } from './params';
import { createTempoCloudWorklog } from './tempo';

// Run this script to copy existing issue from source (Jira Cloud) to target (Jira Cloud) with Tempo worklogs (step 3)
export async function run() {
    const sourceIssue = await JiraCloudSource.Issue.getIssue({ issueIdOrKey: params.source.cloudIssue });
    const sourceWorklogsResponse = await getTempoCloudWorklogs(params.source.cloudIssue);
    console.log(`Source worklogs: ${sourceWorklogsResponse.results.length}`);

    // TODO: implement fetching paged results
    const targetJiraUsers = await JiraCloudTarget.User.getUsers();

    const newTargetIssue = await JiraCloudTarget.Issue.createIssue({
        body: builder => builder
            .withSummary(sourceIssue.fields?.summary!)
            .withProject(params.target.project)
            .withIssueType(sourceIssue.fields?.issuetype?.name!)
    });
    console.log(`New target issue: ${newTargetIssue.key}`);

    // TODO: consider running these operations in parallel
    for (const worklog of sourceWorklogsResponse.results) {
        try {
            console.log(`Copying worklog: ${worklog.description}`);

            const targetUser = targetJiraUsers.find(({ displayName }) => worklog.author.displayName === displayName);
            if (!targetUser) {
                throw new Error(`User not found in target instance: ${worklog.author.displayName}`);
            }

            await createTempoCloudWorklog({
                authorAccountId: targetUser.accountId!,
                billableSeconds: worklog.billableSeconds,
                description: worklog.description,
                issueKey: newTargetIssue.key!,
                startDate: worklog.startDate,
                startTime: worklog.startTime,
                timeSpentSeconds: worklog.timeSpentSeconds
            });
        } catch (e) {
            console.error('Error while copying Tempo worklog', e, worklog);
        }
    }
}

async function getTempoCloudWorklogs(issueKey: string) {
    const response = await fetch(`https://api.tempo.io/core/3/worklogs?issue=${issueKey}`, {
        headers: {
            ...(await getAuthHeaders('source'))
        }
    });

    if (!response.ok) {
        throw new Error(`Error while reading Tempo worklogs: ${response.status} - ${await response.text()}`);
    }

    return await response.json() as GetWorklogsResponse;
}

interface GetWorklogsResponse {
    results: Worklog[];
}

interface Worklog {
    timeSpentSeconds: number;
    billableSeconds: number;
    startDate: string;
    startTime: string;
    description: string;
    author: {
        displayName: string;
    }
}