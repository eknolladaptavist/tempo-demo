import { HttpEndpointRequest, HttpEndpointResponse, buildPlainTextResponse } from '@avst-lib/http-endpoint';
import { State, getMetadata, storeAccessToken, storeRefreshToken } from './storage';

export async function run(request: HttpEndpointRequest): Promise<HttpEndpointResponse | void> {
    try {
        const code = request.queryStringParams['code'];
        const state = request.queryStringParams['state'] as State;

        const metadata = await getMetadata();

        const response = await fetch(`https://api.tempo.io/oauth/token/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: `grant_type=authorization_code&client_id=${metadata.tempo[state].appClientId}&client_secret=${metadata.tempo[state].appClientSecret}&redirect_uri=${metadata.tempo.oauthReplyUrl}&code=${code}`
        });

        if (!response.ok) {
            throw Error(`Invalid response while negotiating tokens: ${response.status} - ${await response.text()}`);
        }

        const { access_token, refresh_token, expires_in }: Tokens = await response.json();
        const expires = Date.now() + expires_in * 1000;

        await storeAccessToken(state, access_token, expires)

        if (refresh_token) {
            await storeRefreshToken(state, refresh_token);
        }

        console.log(`OAuth setup completed, expires: ${new Date(expires).toLocaleString()}`);

        return buildPlainTextResponse('OAuth successfully registered');
    } catch (e) {
        console.error('OAuth reply error', e);
        return buildPlainTextResponse('OAuth error');
    }
}

interface Tokens {
    access_token: string;
    refresh_token: string;
    expires_in: number;
}