import JiraServerSource from './connections/jiraServer/source';
import JiraCloudTarget from './connections/jiraCloud/target';
import { getUserMapping } from './storage';
import { params } from './params';
import { createTempoCloudWorklog } from './tempo';

// Run this script to copy existing issue from source (Jira Server) to target (Jira Cloud) with Tempo worklogs (step 4)
export async function run() {
    const sourceIssue = await JiraServerSource.Issue.getIssue({ issueIdOrKey: params.source.serverIssue });
    console.log(`Source worklogs: ${sourceIssue.fields?.worklog?.worklogs?.length}`);

    const newTargetIssue = await JiraCloudTarget.Issue.createIssue({
        body: builder => builder
            .withSummary(sourceIssue.fields?.summary!)
            .withProject(params.target.project)
            .withIssueType(sourceIssue.fields?.issuetype?.name!)
    });
    console.log(`New target issue: ${newTargetIssue.key}`);

    const userMapping = await getUserMapping();
    console.log(`User mappings: ${userMapping.length}`);

    for (const worklog of sourceIssue.fields?.worklog?.worklogs ?? []) {
        try {
            console.log(`Copying worklog: ${worklog.id}`);
            const { billableSeconds, comment, started, timeSpentSeconds, worker } = await getTempoServerWorklog(worklog.id!);

            const targetUserAccountId = userMapping.find(({ sourceUser }) => worker === sourceUser)?.targetUser;

            if (!targetUserAccountId) {
                throw new Error(`Target user not found for with key: ${worker}`);
            }

            const dateParts = started.split(' ');

            await createTempoCloudWorklog({
                authorAccountId: targetUserAccountId,
                billableSeconds: billableSeconds,
                description: comment,
                issueKey: newTargetIssue.key!,
                timeSpentSeconds: timeSpentSeconds,
                startDate: dateParts[0],
                startTime: dateParts[1].substring(0, dateParts[1].indexOf('.'))
            });
        } catch (e) {
            console.error(`Error while copying worklog: ${worklog.id}`, e);
        }
    }
}

async function getTempoServerWorklog(worklogId: string) {
    const response = await JiraServerSource.fetch(`/rest/tempo-timesheets/4/worklogs/${worklogId}`);

    if (!response.ok) {
        throw new Error(`Error while reading Tempo worklog: ${worklogId} - ${response.status} - ${await response.text()}`);
    }

    return await response.json() as GetWorklogResponse;
}

interface GetWorklogResponse {
    comment: string;
    billableSeconds: number;
    timeSpentSeconds: number;
    started: string;
    worker: string;
}