import { getAuthHeaders } from './connections/tempo';

export async function createTempoCloudWorklog(worklog: CreateWorklogRequest) {
    const response = await fetch('https://api.tempo.io/core/3/worklogs', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json',
            ...(await getAuthHeaders('target'))
        },
        body: JSON.stringify(worklog)
    });

    if (!response.ok) {
        throw new Error(`Error while creating Tempo worklog: ${response.status} - ${await response.text()}`);
    }
}

interface CreateWorklogRequest {
    issueKey: string;
    timeSpentSeconds: number;
    billableSeconds: number;
    startDate: string;
    startTime: string;
    description: string;
    authorAccountId: string;
}