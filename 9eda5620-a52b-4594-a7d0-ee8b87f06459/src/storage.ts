import { createRecordStorage } from '@avst-lib/record-storage';

const AUTH_ACCESS_TOKEN = 'AUTH_ACCESS_TOKEN';
const AUTH_REFRESH_TOKEN = 'AUTH_REFRESH_TOKEN';
const METADATA = 'METADATA';
const USER_MAPPING = 'USER_MAPPING';

interface AccessToken {
    token: string;
    expires: number;
}

interface Metadata {
    tempo: {
        oauthReplyUrl: string;
        source: {
            appClientId: string;
            appClientSecret: string;
        },
        target: {
            appClientId: string;
            appClientSecret: string;
        }
    }
}

export interface UserMapping {
    sourceUser: string;
    targetUser: string;
}

export type State = 'source' | 'target';

const storage = createRecordStorage();

export async function getMetadata() {
    const metadata = await storage.getValue<Metadata>(METADATA);

    if (!metadata) {
        throw new Error('Metadata not found, have you set up metadata?');
    }

    return metadata;
}

export async function getAccessToken(state: State) {
    const accessToken = await storage.getValue<AccessToken>(`${AUTH_ACCESS_TOKEN}:${state}`);

    if (!accessToken) {
        throw new Error('Access not found, you probably haven\'t set up OAuth connection');
    }

    return accessToken;
}

export async function getRefreshToken(state: State) {
    const refreshToken = await storage.getValue<string>(`${AUTH_REFRESH_TOKEN}:${state}`);

    if (!refreshToken) {
        throw new Error('Refresh token missing, you probably haven\'t set up OAuth connection');
    }

    return refreshToken;
}

export async function storeAccessToken(state: State, token: string, expires: number) {
    await storage.setValue(`${AUTH_ACCESS_TOKEN}:${state}`, {
        token,
        expires
    } as AccessToken);
}

export async function storeRefreshToken(state: State, token: string) {
    await storage.setValue(`${AUTH_REFRESH_TOKEN}:${state}`, token);
}

export async function storeMetadata(metadata: Metadata) {
    await storage.setValue(METADATA, metadata);
}

export async function storeUserMapping(userMapping: UserMapping[]) {
    await storage.setValue(USER_MAPPING, userMapping);
}

export async function getUserMapping(): Promise<UserMapping[]> {
    return (await storage.getValue(USER_MAPPING)) ?? [];
}