import { getMetadata, getAccessToken, getRefreshToken, storeAccessToken, State } from '../storage';

export async function getAuthHeaders(state: State): Promise<Record<string, string>> {
    const accessToken = await getAccessToken(state);

    if (Date.now() > accessToken.expires - 100000) {
        console.log(`Access token is going to expire in ${(accessToken.expires - Date.now()) / 1000} seconds, negotiation new access token`);
        return {
            Authorization: `Bearer ${await refreshAccessToken(state)}`
        }
    } else {
        console.log(`Using existing access token, which will expire in ${(accessToken.expires - Date.now()) / 1000} seconds`);
        return {
            Authorization: `Bearer ${accessToken.token}`
        }
    }
}

async function refreshAccessToken(state: State) {
    const refreshToken = await getRefreshToken(state);
    const metadata = await getMetadata();

    const response = await fetch(`https://api.tempo.io/oauth/token/`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: `grant_type=refresh_token&client_id=${metadata.tempo[state].appClientId}&client_secret=${metadata.tempo[state].appClientSecret}&redirect_uri=${metadata.tempo.oauthReplyUrl}&refresh_token=${refreshToken}`
    });

    if (!response.ok) {
        throw Error(`Invalid response ${response.status}: ${await response.text()}`);
    }

    const { access_token, expires_in }: RefreshTokenResponse = await response.json();
    const expires = Date.now() + expires_in * 1000;
    await storeAccessToken(state, access_token, expires);

    console.log(`OAuth connection refreshed, expires: ${new Date(expires).toLocaleString()}`);

    return access_token;
}

interface RefreshTokenResponse {
    access_token: string;
    expires_in: number;
}